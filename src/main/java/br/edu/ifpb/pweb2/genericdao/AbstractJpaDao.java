package br.edu.ifpb.pweb2.genericdao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public abstract class AbstractJpaDao<T extends Serializable, FK> implements IGenericDao<T, FK> {

	private Class<T> clazz;

	@PersistenceContext
	protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public AbstractJpaDao() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazz = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findById(FK id) {
		return entityManager.find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	@Transactional
	public void save(T entity) {
		entityManager.persist(entity);
	}

	@Transactional
	public T update(T entity) {
		entityManager.merge(entity);
		return entity;
	}

	@Transactional
	public void delete(T entity) {
		entityManager.remove(entity);
	}

	@Transactional
	public void deleteById(FK entityId) {
		T entity = findById(entityId);
		delete(entity);
	}
}