package br.edu.ifpb.pweb2.genericdao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T extends Serializable, FK> {

	T findById(FK id);

	List<T> findAll();

	void save(final T entity);

	T update(final T entity);

	void delete(final T entity);

	void deleteById(final FK entityId);
}
