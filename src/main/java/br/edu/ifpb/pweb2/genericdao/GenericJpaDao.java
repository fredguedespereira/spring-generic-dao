package br.edu.ifpb.pweb2.genericdao;

import java.io.Serializable;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class GenericJpaDao< T extends Serializable, FK >
 extends AbstractJpaDao< T, FK > implements IGenericDao< T, FK >{
   //
}
